package com.nanorep.utils;

import android.util.Log;

import com.nanorep.interfaces.RssApiInterface;
import com.nanorep.models.RssAnswer;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Created by Noa on 12/09/2016.
 */
public class RssApiClient {
    private static final String TAG = "RSS API CLIENT";
    private static RssApiClient rssApiClient;
    private RssApiInterface rssApiInterface;

    public static RssApiClient init() {

        if (rssApiClient == null)
            rssApiClient = new RssApiClient();

        return rssApiClient;
    }

    private RssApiClient() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.globes.co.il")
                .client(new OkHttpClient())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();

        rssApiInterface = retrofit.create(RssApiInterface.class);
    }

    public Call<RssAnswer> getRssInfo(String id) {

        Log.d(TAG, "get rss info started");

        return rssApiInterface.getRssInfo(id);
    }
}
