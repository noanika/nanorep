package com.nanorep.interfaces;

import com.nanorep.models.RssAnswer;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Noa on 12/09/2016.
 */
public interface RssApiInterface {

    @GET("/WebService/Rss/RssFeeder.asmx/FeederNode")
    Call<RssAnswer> getRssInfo(@Query("iID") String id);
}
