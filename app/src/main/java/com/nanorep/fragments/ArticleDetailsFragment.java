package com.nanorep.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nanorep.R;
import com.nanorep.activities.MainActivity;
import com.nanorep.databinding.ArticleDetailsFragmentBinding;
import com.nanorep.models.FeedItem;

import butterknife.ButterKnife;

/**
 * Created by Noa on 12/09/2016.
 */
public class ArticleDetailsFragment extends Fragment {

    public static final String TAG = ArticleDetailsFragment.class.getName();
    private static final String ARG_FEED_ITEM = "feedItem";

    private FeedItem feedItem;
    private int pos;
    private ArticleDetailsFragmentBinding binding;

    public static ArticleDetailsFragment newInstance(FeedItem feedItem) {
        ArticleDetailsFragment fragment = new ArticleDetailsFragment();

        Bundle args = new Bundle();

        args.putParcelable(ARG_FEED_ITEM, feedItem);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        // update the actionbar to show the up carat/affordance
        ((MainActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getArguments() != null) {

            feedItem = getArguments().getParcelable(ARG_FEED_ITEM);

            Log.d(TAG, "onCreate: received article === " + feedItem );
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.article_details_fragment, container, false);

//        setHasOptionsMenu(true);
        ((MainActivity) getActivity()).setActionBarTitle(feedItem.getMtitle());

        //binding
        binding = DataBindingUtil.bind(rootView);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        binding.setFeedItem(feedItem);
    }


}
