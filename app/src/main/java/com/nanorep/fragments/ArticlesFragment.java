package com.nanorep.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nanorep.R;
import com.nanorep.adapters.ArticlesAdapter;
import com.nanorep.dividers.DividerItemDecoration;
import com.nanorep.models.RssAnswer;
import com.nanorep.utils.Const;
import com.nanorep.utils.RssApiClient;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import butterknife.BindView;

/**
 * Created by Noa on 12/09/2016.
 */
public class ArticlesFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    public static final String TAG = ArticlesFragment.class.getName();
    ArticlesAdapter adapter;

    @BindView(R.id.articles_recycler)
    RecyclerView articlesRecycler;

    @BindView(R.id.articles_refresher)
    SwipeRefreshLayout articlesRefresher;

    public static ArticlesFragment newInstance() {

        Bundle args = new Bundle();

        ArticlesFragment fragment = new ArticlesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new ArticlesAdapter(getActivity());

        loadArticles(Const.RSS_ID);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.articles_fragment, container, false);

        //Binding methods
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        articlesRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        articlesRecycler.setAdapter(adapter);
        articlesRecycler.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));

        articlesRefresher.setOnRefreshListener(this);
    }

    public void loadArticles(String id) {

        adapter.clearList();

        RssApiClient rssApiClient = RssApiClient.init();

        Call<RssAnswer> call = rssApiClient.getRssInfo(id);

        call.enqueue(new Callback<RssAnswer>() {
            @Override
            public void onResponse(Call<RssAnswer> call, Response<RssAnswer> response) {

                Log.d(TAG, "onResponse: " + "loaded articles = " + response.body());

                if (response.body() != null) {
                    adapter.addItems(response.body().getmChannel().getFeedItems());
                    articlesRefresher.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<RssAnswer> call, Throwable t) {

                Log.e(TAG, "onFailure: load articles" + Log.getStackTraceString(t) );

                articlesRefresher.setRefreshing(false);
            }
        });
    }

    @Override
    public void onRefresh() {
        loadArticles(Const.RSS_ID);
    }
}
