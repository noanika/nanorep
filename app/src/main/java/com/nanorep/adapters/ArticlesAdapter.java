package com.nanorep.adapters;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nanorep.R;
import com.nanorep.databinding.ArticleItemListBinding;
import com.nanorep.fragments.ArticleDetailsFragment;
import com.nanorep.fragments.ArticlesFragment;
import com.nanorep.interfaces.CRUDAdapterInterface;
import com.nanorep.models.FeedItem;
import com.nanorep.utils.FragmentUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Noa on 12/09/2016.
 */
public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.ViewHolder>
        implements CRUDAdapterInterface<FeedItem> {

    private static final String TAG = ArticlesAdapter.class.getName();
    Context context;
    LayoutInflater layoutInflater;
    List<FeedItem> feedItems;

    public ArticlesAdapter(Context context) {

        this.context = context;
        this.feedItems = new ArrayList<>();
        this.layoutInflater = LayoutInflater.from(this.context);
    }

    @Override
    public ViewHolder  onCreateViewHolder(ViewGroup parent, int viewType) {
        ArticleItemListBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.article_item_list,
                parent, false);

        return new ViewHolder(binding);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ArticleItemListBinding binding;

        public ViewHolder(ArticleItemListBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final FeedItem feedItem = getItem(position);
        final int pos = position - 1;

        Log.d(TAG, "onBindViewHolder: current feedItem answer = " + feedItem);

        //bind album to view
        holder.binding.setFeedItem(feedItem);

        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showItem(feedItem, pos);
            }
        });
    }

    @Override
    public int getItemCount() {
        return feedItems.size();
    }


    @Override
    public FeedItem getItem(int position) {
        return feedItems.get(position);
    }

    @Override
    public void addItem(FeedItem item) {

    }

    @Override
    public void removeItem(FeedItem item) {

    }

    @Override
    public void addItems(List<FeedItem> items) {
        feedItems.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public void clearList() {
        feedItems.clear();
        notifyDataSetChanged();
    }

    @Override
    public void updateItem(int pos, FeedItem item) {

    }

    @Override
    public void showItem(FeedItem item, int itemPosition) {
        ArticleDetailsFragment articleDetailsFragment = ArticleDetailsFragment.newInstance(item);

        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        ArticlesFragment articlesFragment = (ArticlesFragment)fragmentManager.findFragmentByTag(ArticlesFragment.TAG);


        FragmentUtils.addFragment(articlesFragment, articleDetailsFragment, R.id.content_main, context);
    }

    @Override
    public List<FeedItem> getItems() {
        return feedItems;
    }

    @Override
    public void removeItem(int currentItemPosition) {

    }
}
