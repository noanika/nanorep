package com.nanorep.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/**
 * Created by Noa on 12/09/2016.
 */
@Root(name = "rss", strict = false)
public class RssAnswer implements Serializable {

    @Element(name = "channel")
    private Channel mChannel;

    public Channel getmChannel() {
        return mChannel;
    }

    public RssAnswer() {
    }

    public RssAnswer(Channel mChannel) {
        this.mChannel = mChannel;
    }
}
