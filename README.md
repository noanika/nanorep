# README #

shows a list of articles from Globes RSS.

I used RetroFit with simplexml converter in order to get the RSS from internet and parse it.

To bind the application logic and layouts i used Android data binding library,
also used ButterKnife lib to bind the views to their objects in fragments.

You can pull to refresh in articles fragment to update RSS.